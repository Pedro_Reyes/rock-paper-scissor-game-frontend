import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { User } from './model/user.model';
import { Round } from './model/round.model';
import { LocalGameReport } from './model/localGameReport.model';
import { GlobalGameReport } from './model/globalGameReport.model';
import { GameApiService } from './service/game/game-api.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'rock-paper-scissors';

  user: User;

  constructor(
    public gameAPIService: GameApiService,
    private _snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    // Authenticating temporal session
    this.gameAPIService.generateSingleSessionAuthentication();
    this.gameAPIService.user.subscribe(value => {
      this.user = value;
    });

    this.gameAPIService.userError.subscribe(error => {
      this.openSnackBar(error);
    });
  }

  /**
   * Play a round for the current game and triggers the local report
   */
  playRound(): void {
    this.gameAPIService.playRound();
  }

  /**
   * Restart the current game
   */
  restartGame(): void {
    this.gameAPIService.restartGame();
  }

  private openSnackBar(message: string): void {
    this._snackBar.open(message, undefined, {
      duration: this.gameAPIService.SNACK_BAR_DURATION
    });
  }
}
