export interface GlobalGameReport {
  totalRoundsPlayed: number;
  totalWinsForFirstPlayer: number;
  totalWinsForSecondPlayer: number;
  totalDraws: number;
}
