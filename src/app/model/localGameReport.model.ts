import { Round } from './round.model';

export interface LocalGameReport {
  roundFromCurrentGame: Round[];
}
