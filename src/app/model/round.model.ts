export interface Round {
  player1Movement: string;
  player2Movement: string;
  roundResult: string;
  reset?: boolean;
}
