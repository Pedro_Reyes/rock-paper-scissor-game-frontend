import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocalGameReportComponent } from './local-game-report.component';

describe('LocalGameReportComponent', () => {
  let component: LocalGameReportComponent;
  let fixture: ComponentFixture<LocalGameReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocalGameReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalGameReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
