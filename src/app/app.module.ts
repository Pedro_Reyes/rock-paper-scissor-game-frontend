// Default
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Material
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatChipsModule } from '@angular/material/chips';
import { MatSnackBarModule } from '@angular/material/snack-bar';

// Http client
import { HttpClientModule } from '@angular/common/http';

// Components
import { AppComponent } from './app.component';
import { GlobalGameReportComponent } from './global-game-report/global-game-report.component';
import { LocalGameReportComponent } from './local-game-report/local-game-report.component';

@NgModule({
  declarations: [
    AppComponent,
    LocalGameReportComponent,
    GlobalGameReportComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NoopAnimationsModule,
    BrowserAnimationsModule,

    MatButtonModule,
    MatTableModule,
    MatCardModule,
    MatListModule,
    MatChipsModule,
    MatSnackBarModule,

    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
