import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { User } from '../../model/user.model';
import { Round } from '../../model/round.model';
import { LocalGameReport } from '../../model/localGameReport.model';
import { GlobalGameReport } from '../../model/globalGameReport.model';
import { Observable, BehaviorSubject, interval } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class GameApiService {
  // API Header & Port
  HEADER: string = '{"responseType": "text"}';
  API_PORT: string = '8080';

  // API ENDPOINTS
  API_URL_ROOT: string = 'http://localhost:' + this.API_PORT;
  API_URL_GENERATE_UID: string = this.API_URL_ROOT + '/game/generate-uid';
  API_URL_PLAY_ROUND: string = this.API_URL_ROOT + '/game/play-round';
  API_URL_RESTART_GAME: string = this.API_URL_ROOT + '/game/restart';
  API_URL_REPORT: string = this.API_URL_ROOT + '/game/report';
  HTTP_OPTIONS = {
    headers: new HttpHeaders({
      responseType: 'text'
    })
  };

  // API results
  user: BehaviorSubject<User> = new BehaviorSubject<User>(undefined);
  localGameReport: BehaviorSubject<LocalGameReport> = new BehaviorSubject<
    LocalGameReport
  >(undefined);
  globalGameReport: BehaviorSubject<GlobalGameReport> = new BehaviorSubject<
    GlobalGameReport
  >(undefined);

  userError: BehaviorSubject<string> = new BehaviorSubject<string>(undefined);
  INTERVAL_FOR_CONNECTION_FAIL: number = 10_000;
  SNACK_BAR_DURATION: number = 10_000;
  INTERVAL_FOR_NEW_GAME_REPORT: number = 1_000;

  constructor(public http: HttpClient, private _snackBar: MatSnackBar) {
    // Check periodically the internet connection
    this.gamesReport(true);
    interval(this.INTERVAL_FOR_CONNECTION_FAIL).subscribe(() =>
      this.gamesReport(true)
    );

    // Initialize global report with current statistics
    setInterval(() => this.gamesReport(), this.INTERVAL_FOR_NEW_GAME_REPORT);
  }

  /**
   * Play another round in the current game for the current session
   */
  public playRound(): void {
    if (this.user.value) {
      this.http
        .post<string>(this.API_URL_PLAY_ROUND, this.user.value)
        .toPromise()
        .then(result => {
          this.localGameReport.next(JSON.parse(JSON.stringify(result)));
        })
        .catch(error => {});
    }
  }

  /**
   * Restart the game resetting the number rounds played to zero
   */
  public restartGame(): void {
    this.http
      .post<string>(this.API_URL_RESTART_GAME, this.user.value)
      .toPromise()
      .then(result => {
        this.localGameReport.next(JSON.parse(JSON.stringify(result)));
      })
      .catch(error => {});
  }

  /**
   * Generates a report of all the games played
   */
  public gamesReport(enableNotifications?: boolean): void {
    this.http.get(this.API_URL_REPORT, this.HTTP_OPTIONS).subscribe(
      resp => {
        this.globalGameReport.next(JSON.parse(JSON.stringify(resp)));

        if (this.userError.value) {
          this.generateSingleSessionAuthentication();
          this.userError.next(undefined);
        }
      },
      err => {
        if (enableNotifications) {
          this.userError.next(
            'Please, verify your connection or wait for our server to work again'
          );
        }
      }
    );
  }

  /**
   * Provides an unique way to identify current user playing. However, this
   * authentication will expire once the user closes the tab.
   */
  public generateSingleSessionAuthentication(): void {
    this.http
      .get(this.API_URL_GENERATE_UID, { responseType: 'text' })
      .toPromise()
      .then(result => {
        this.user.next(JSON.parse(result) as User);
      })
      .catch(error => {});
  }
}
