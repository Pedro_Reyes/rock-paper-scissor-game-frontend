import { Component, OnInit } from '@angular/core';
import { GlobalGameReport } from '../model/globalGameReport.model';
import { GameApiService } from '../service/game/game-api.service';

@Component({
  selector: 'app-global-game-report',
  templateUrl: './global-game-report.component.html',
  styleUrls: ['./global-game-report.component.css']
})
export class GlobalGameReportComponent implements OnInit {
  globalGameReport: GlobalGameReport;

  constructor(public gameAPIService: GameApiService) {}

  ngOnInit() {
    this.gameAPIService.globalGameReport.subscribe(value => {
      this.globalGameReport = value;
    });
  }
}
